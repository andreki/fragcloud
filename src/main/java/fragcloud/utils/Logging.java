package fragcloud.utils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.logging.*;

public class Logging {
    private static final Level LOG_LEVEL = Level.INFO;
    private static final String OUTPUT_FNAME = "output.log";
    private static final int OUTPUT_FLIMIT_BYTES = 1000000; // 1 Mb
    private static final String LOGGER_NAME = "fragcloud";

    private static Logger logger;

    private static boolean systemLogging = true; // System console output
    private static boolean fileLogging = true; // File output
    private static boolean uiLogging = true; // UI output

    private static FileHandler fileHandler;
    private static StreamHandler systemHandler;

    private static Formatter formatter;


    public static void fine(String message, Object... params) {
        log(Level.FINE, message, params);
    }

    public static void info(String message, Object... params) {
        log(Level.INFO, message, params);
    }

    public static void warning(String message, Object... params) {
        log(Level.WARNING, message, params);
    }

    public static void error(String message, Object... params) {
        log(Level.SEVERE, message, params);
    }

    public static void log(Level level, String message, Object... params) {
        if (logger == null) createLogger();
        message = String.format(message, params); // String format first
        logger.log(level, message);
    }

    private static void createLogger() {
        // Add to logger
        logger = Logger.getLogger(LOGGER_NAME);
        logger.setLevel(LOG_LEVEL);
        logger.setUseParentHandlers(false);
        updateAllHandlers();
    }

    private static Formatter getFormatter() {
        if (formatter == null) formatter = new Formatter() {
            @Override
            public String format(LogRecord record) {
                return String.format("%s%s", record.getMessage(), System.lineSeparator());
            }
        };
        return formatter;
    }

    private static void updateAllHandlers() {
        updateFileHandler();
        updateSystemHandler();
        updateUIHandler();
    }

    private static void updateUIHandler() {
        // TODO:
    }

    private static void updateSystemHandler() {
        if (systemLogging) {
            if (systemHandler == null) systemHandler = new StreamHandler(System.out, getFormatter());
//            if (systemHandler == null) systemHandler = new StreamHandler(System.out, new SimpleFormatter());
//            if (systemHandler == null) systemHandler = new ConsoleHandler();
            logger.addHandler(systemHandler);
        }
        else if (systemHandler != null) {
            logger.removeHandler(systemHandler);
            systemHandler.close();
            systemHandler = null;
        }
    }

    private static void updateFileHandler() {
        if (fileLogging) {
            if (fileHandler == null) try {
                fileHandler = new FileHandler(OUTPUT_FNAME, OUTPUT_FLIMIT_BYTES, 1, true);
                fileHandler.setFormatter(getFormatter());
                logger.addHandler(fileHandler);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (fileHandler != null) {
            logger.removeHandler(fileHandler);
            fileHandler.close();
            fileHandler = null;
        }
    }

    public static void setSystemLog(boolean state) {
        systemLogging = state;
        updateSystemHandler();
    }

    public static void setFileLog(boolean state) {
        fileLogging = state;
        updateFileHandler();
    }

    public static void setUILog(boolean state) {
        uiLogging = state;
        // TODO:
    }
}
