package fragcloud.utils;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static Field getFieldExtended(Class<?> clazz, String fieldName) {
        Class<?> current = clazz;
        do {
            try {
                return current.getDeclaredField(fieldName);
            } catch(Exception e) {}
        } while((current = current.getSuperclass()) != null);
        return null;
    }

    public static List<Field> getFieldsExtended(Class<?> clazz) {
        List<Field> fields = new ArrayList<>();
        Class<?> current = clazz;
        do {
            try {
                Field[] currentFields = current.getDeclaredFields();
                if (currentFields.length > 0) {
                    for (Field currentField : currentFields) {
                        fields.add(currentField);
                    }
                }
            } catch(Exception e) {}
        } while((current = current.getSuperclass()) != null);
        return fields;
    }

    public static String computeMd5(File file) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[1024];
            int read = 0;
            InputStream is = new FileInputStream(file);
            while( (read=is.read(buffer)) != -1){
                md.update(buffer,0,read);
            }
            byte[] digest = md.digest();
            String md5 =DatatypeConverter.printHexBinary(digest);
            return md5;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T invokeMethodFor(Class clazz, String methodName, Object... args) {
        try {
            Method method = clazz.getMethod(methodName);
            return (T) method.invoke(args);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

}
