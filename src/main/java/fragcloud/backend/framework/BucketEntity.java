package fragcloud.backend.framework;

public abstract class BucketEntity extends BaseEntity {
    // Database Fields
    @DatabaseColumn
    protected String credentials;

    @DatabaseColumn
    protected String clazz;

    public static String getTableName() {
        return "bucket";
    }

    public abstract void authorize();

    public abstract void listen();

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    public abstract boolean uploadFile(FileEntity file);

    public abstract long getStorageUsed();

    public abstract long getStorageTotal();

    public abstract long getStorageRemaining();
}
