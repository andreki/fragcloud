package fragcloud.backend.cloud;

import fragcloud.backend.framework.BucketEntity;
import fragcloud.backend.framework.FileEntity;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

public class CloudService {
    private static final String BUCKET_NAMESPACE = "fragcloud.backend.cloud.buckets.";

    private static CloudService instance;
    private List<BucketEntity> buckets = new ArrayList<>();

    private CloudService() {
    }

    public static CloudService getInstance() {
        if (instance == null) instance = new CloudService();
        return instance;
    }

    public void addBuckets() {
        List<BucketEntity> buckets = BucketEntity.getAllWhere("1", BucketEntity.class); // TODO: create from class name (GoogleDriveBucket)

        for (BucketEntity bucket : buckets) {
            //        bucket.authorize();
            bucket.listen();
        }

        this.buckets.addAll(buckets);
    }

    private BucketEntity getBucketWithSize(long minBytes) throws Exception {
        if (buckets.isEmpty()) throw new Exception("No buckets registered!");

        for (BucketEntity bucket : buckets) {
            if (bucket.getStorageRemaining() > minBytes) return bucket;
        }

        throw new Exception("No bucket with enough storage found!");
    }

    public void uploadFile(FileEntity file) throws Exception {
        // TODO: get bucket with size
        BucketEntity bucket = getBucketWithSize(file.getSize(false));

        // TODO: upload the file to remote
        boolean success = bucket.uploadFile(file);

        // TODO: remember what bucket the file is stored to -- this should save as `file.cloud`
    }

    public List<BucketEntity> getBuckets() {
        return buckets;
    }
}
