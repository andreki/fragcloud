package fragcloud.backend.sync.watchers;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import static java.nio.file.StandardWatchEventKinds.*;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.attribute.BasicFileAttributes;

public class RecursiveWatchServiceDirectoryWatcher extends BaseDirectoryWatcher {

    private final WatchService watchService;
    private final Map<WatchKey, Path> keyPathMap = new HashMap<>();
    private final WatchEvent.Kind[] eventKinds;
    private final boolean all;
    private Path currentFilePath;


    public RecursiveWatchServiceDirectoryWatcher() {
        try {
            this.watchService = FileSystems.getDefault().newWatchService();
        } catch (IOException ex) {
            throw new RuntimeException("Cannot create watch service. Reason: ", ex);
        }
        this.eventKinds = new WatchEvent.Kind[]{
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_DELETE,
                StandardWatchEventKinds.ENTRY_MODIFY};
        this.all = true;
    }

    public void create(String directory) {
        super.create(directory);

        try {
            Path path = Paths.get(directory);
            registerPath(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This blocking method continously watch given directories until an
     * exception throw or it have no job to do anymore (ex: folder deleted,
     * permission changed...)
     *
     * @throws IOException
     * @throws InterruptedException
     */
    private void watch() throws IOException, InterruptedException {
        MAIN_LOOP:
        while (true) {
            final WatchKey key = watchService.take();

            INNER_LOOP:
            for (WatchEvent<?> watchEvent : key.pollEvents()) {
                final Kind<?> kind = watchEvent.kind();

                WatchEvent<Path> watchEventPath = (WatchEvent<Path>) watchEvent;
                Path fileName = watchEventPath.context();
                Path parentDir = keyPathMap.get(key);
                Path child = parentDir.resolve(fileName);
                setCurrentFilePath(child);

                if (kind == ENTRY_CREATE) {
                    if (Files.isDirectory(child, LinkOption.NOFOLLOW_LINKS) && all) {
                        registerDir(child);
                    }
                    trigger(FileEvent.FILE_CREATED, watchEventPath.context().toString());
                    continue;
                }

                if (kind == ENTRY_MODIFY) {
                    trigger(FileEvent.FILE_MODIFIED, watchEventPath.context().toString());
                    continue;
                }

                if (kind == ENTRY_DELETE) {
                    trigger(FileEvent.FILE_DELETED, watchEventPath.context().toString());
                }
            }

            boolean validKey = key.reset();

            if (!validKey) {
                keyPathMap.remove(key);
                if (keyPathMap.isEmpty()) {
                    watchService.close();
                    break MAIN_LOOP;
                }
            }
        }
    }


    /**
     * Register a directory to watch
     *
     * @param dir Path, the Path object represent the directory
     * @throws IOException
     */
    public void registerDir(Path dir) throws IOException {
        if (Files.isDirectory(dir, LinkOption.NOFOLLOW_LINKS)) {
            if (all) {
                Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                            throws IOException {
                        registerPath(dir);
                        return FileVisitResult.CONTINUE;
                    }

                    public FileVisitResult visitFileFailed(Path file, IOException exc)
                            throws IOException {
                        return FileVisitResult.CONTINUE;
                    }
                });
            } else registerPath(dir);
        }
    }

    private void setCurrentFilePath(Path file) {
        currentFilePath = file;
    }

    /**
     *
     * @return the Path object of the file which is making current event
     */
    public Path getCurrentFilePath() {
        return currentFilePath;
    }

    private void registerPath(Path path) throws IOException {
        WatchKey key = path.register(watchService, eventKinds);
        keyPathMap.put(key, path);
    }

    @Override
    public void run() {
        try {
            watch();
        } catch (IOException | InterruptedException ex) {
            throw new RuntimeException(ex);
        }
    }
}
