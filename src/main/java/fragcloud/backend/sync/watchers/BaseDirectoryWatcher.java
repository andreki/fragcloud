package fragcloud.backend.sync.watchers;

import fragcloud.backend.database.BaseDatabase;
import fragcloud.utils.Logging;
import fragcloud.utils.Stopwatch;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.EnumSet;

import static java.nio.file.FileVisitOption.FOLLOW_LINKS;

public abstract class BaseDirectoryWatcher implements Runnable {
    private Path path;

    public void create(String directory) {
        this.path = Paths.get(directory);
    }

    private String getWhereSql() {
        return "directory = '"+path.toAbsolutePath()+"'";
    }

    private Long getLastSynced() {
        Long lastSync = BaseDatabase.getInstance().getItemColumn("select lastSync from directory where " + getWhereSql(), Long.class);
        return lastSync;
    }

    private Long getLastModified() {
        Long lastModified = path.toFile().lastModified();
       return lastModified;
    }

    public final void sync(boolean force) {
        Stopwatch stopwatch = new Stopwatch("Syncing %s...", getAbsolutePath());

        try {
            if (__sync(force)) {
                stopwatch.end("Syncing %s done", getAbsolutePath());
            }
            else {
                Logging.warning("Sync skipped for %s, has either not been updated since last sync or is not set to force sync", getAbsolutePath());
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    private final boolean __sync(boolean force) throws IOException {
        Long lastSynced = getLastSynced();
        Long lastModified = getLastModified();
        if (!force && (lastSynced != null && lastModified != null && lastSynced >= lastModified)) {
            return false;
        }

//        BaseDatabase.getInstance().startTransaction();

        UploadVisitor pf = new UploadVisitor();
        EnumSet<FileVisitOption> opts = EnumSet.of(FOLLOW_LINKS);
        Files.walkFileTree(path, opts, Integer.MAX_VALUE, pf);

        BaseDatabase.getInstance().queryUpdate("update directory set lastSync="+System.currentTimeMillis()+" where " + getWhereSql());
//        BaseDatabase.getInstance().endTransaction();

        return true;
    }

    public final void trigger(FileEvent event, String file) {
        switch (event) {
            case FILE_CREATED:
                Logging.info("Created: " + file);
                break;
            case FILE_DELETED:
                Logging.info("Delete: " + file);
                break;
            case FILE_MODIFIED:
                Logging.info("Modify: " + file);
                break;
        }
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public String getAbsolutePath() {
        return getPath().toFile().getAbsolutePath();
    }
}
