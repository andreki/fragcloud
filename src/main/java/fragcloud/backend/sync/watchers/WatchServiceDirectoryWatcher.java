package fragcloud.backend.sync.watchers;

import fragcloud.utils.Logging;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;

public class WatchServiceDirectoryWatcher extends BaseDirectoryWatcher {

    private WatchKey watchKey;
    private WatchService watcher;

    @Override
    public void create(String directory) {
        super.create(directory);

        //define a folder root
        Path myDir = Paths.get(directory);

        try {
            watcher = myDir.getFileSystem().newWatchService();
            myDir.register(watcher,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_DELETE,
                    StandardWatchEventKinds.ENTRY_MODIFY);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                iterate();
            }
        } catch (Exception e) {
            Logging.info("Error: " + e.toString());
        }
    }

    private void iterate() throws InterruptedException {
        watchKey = watcher.take();
        List<WatchEvent<?>> events = watchKey.pollEvents();
        for (WatchEvent event : events) {
            String directory = event.context().toString();
            if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
                trigger(FileEvent.FILE_CREATED, directory);
            }
            if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE) {
                trigger(FileEvent.FILE_DELETED, directory);
            }
            if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
                trigger(FileEvent.FILE_MODIFIED, directory);
            }
        }
        watchKey.reset();
    }

}
