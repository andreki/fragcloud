package fragcloud.backend.sync.watchers;

import net.contentobjects.jnotify.JNotify;
import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;

public class JNotifyDirectoryWatcher extends BaseDirectoryWatcher {
    @Override
    public void create(String directory) {
        super.create(directory);
    }

    @Override
    public void run() {
        try {
            // watch mask, specify events you care about,
            // or JNotify.FILE_ANY for all events.
            int mask =  JNotify.FILE_CREATED  |
                    JNotify.FILE_DELETED  |
                    JNotify.FILE_MODIFIED |
                    JNotify.FILE_RENAMED;

            // watch subtree?
            boolean watchSubtree = true;

            // add actual watch
            int watchID = 0;
            watchID = JNotify.addWatch(getPath().toFile().getAbsolutePath(), mask, watchSubtree, getListener());

            // sleep a little, the application will exit if you
            // don't (watching is asynchronous), depending on your
            // application, this may not be required
            Thread.sleep(1000000);

            // to remove watch the watch
            boolean res = false;
            res = JNotify.removeWatch(watchID);
            if (!res) {
                // invalid watch ID specified.
            }
        } catch (JNotifyException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private JNotifyListener getListener() {
        return new JNotifyListener() {
            @Override
            public void fileCreated(int i, String rootPath, String name) {
                trigger(FileEvent.FILE_CREATED, name);
            }

            @Override
            public void fileDeleted(int i, String rootPath, String name) {
                trigger(FileEvent.FILE_DELETED, name);
            }

            @Override
            public void fileModified(int i, String rootPath, String name) {
                trigger(FileEvent.FILE_MODIFIED, name);
            }

            @Override
            public void fileRenamed(int i, String rootPath, String name, String oldName) {
                trigger(FileEvent.FILE_RENAMED, name);
            }
        };
    }
}
