package fragcloud.backend.sync;

import fragcloud.backend.framework.DirectoryEntity;
import fragcloud.backend.sync.watchers.BaseDirectoryWatcher;
import fragcloud.backend.sync.watchers.RecursiveWatchServiceDirectoryWatcher;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SyncService {
    private ExecutorService executor;

    public SyncService() {
        executor = Executors.newCachedThreadPool();
    }

    public void addFileWatchers() {
        List<DirectoryEntity> directories = DirectoryEntity.getAllWhere("1", DirectoryEntity.class);
        for (DirectoryEntity directory : directories) {
            addFileWatcher(directory.getDirectory());
        }
    }

    public void addFileWatcher(String directory) {
        BaseDirectoryWatcher watcher = new RecursiveWatchServiceDirectoryWatcher();
        watcher.create(directory);
        watcher.sync(true);
        executor.submit(watcher);
    }
}
