package fragcloud.backend.database;

import java.util.List;
import java.util.Map;

public abstract class BaseDatabase {
    public abstract void initiate();

    public abstract List<Map<String, Object>> getItems(String query);
    public abstract Map<String, Object> getItem(String query);
    public abstract <T> T getItemColumns(String query, Class<T> type);
    public abstract <T> T getItemColumn(String query, Class<T> type);

    public abstract boolean query(String query);
    public abstract void queryUpdate(String query);
    public abstract int queryInsert(String query);

    public abstract void startTransaction();
    public abstract void endTransaction();
    public abstract void rollback();

    public abstract void reset();

    // Singleton
    private static BaseDatabase database;

    public static BaseDatabase getInstance() {
        if (database == null) database = new SqlLiteDatabase();
        return database;
    }
}