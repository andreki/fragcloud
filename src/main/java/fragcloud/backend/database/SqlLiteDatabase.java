package fragcloud.backend.database;

import fragcloud.utils.Logging;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SqlLiteDatabase extends BaseDatabase {
    private final int CONNECTION_VALIDATION_TIMEOUT = 5000;
    private final String DATABASE_NAME = "./src/main/resources/public/database.sqlite";
    private final String DATABASE_INSTALL_NAME = "./src/main/resources/sqlite_installer.sql";

    private Connection connection;

    @Override
    public void initiate() {
        getConnection();
    }

    private Connection getConnection() {
        try {
            if (connection == null || !connection.isValid(CONNECTION_VALIDATION_TIMEOUT)) {
                connection = DriverManager.getConnection("jdbc:sqlite:"+DATABASE_NAME);
                if (!__isInstalled()) {
                    __install();
                }
            }
        } catch (SQLException e) {
            Logging.warning("Issue with connecting sqlite: " + e.getMessage());
            e.printStackTrace();
        }
        return connection;
    }

    private boolean __isInstalled() {
        return getItemColumn("SELECT COUNT(*) FROM sqlite_master WHERE type='table' and name IN('file','directory','bucket')", Integer.class) == 3;
    }

    private void __install() {
        Logging.info("Performing first-time sqlite install..");
        startTransaction();
        try {
            __resetDatabase();
            endTransaction();
        } catch (SQLException e) {
            rollback();
            Logging.error("Database install failed!");
            e.printStackTrace();
        }
        Logging.info("Database install done!");
    }


    private void __resetDatabase() throws SQLException
    {
        __executeSqlFile(DATABASE_INSTALL_NAME);
    }

    private void __executeSqlFile(String fname) {
        String s            = new String();
        StringBuffer sb = new StringBuffer();

        try {
            FileReader fr = new FileReader(new File(fname));

            BufferedReader br = new BufferedReader(fr);

            while((s = br.readLine()) != null) {
                s = s.replaceAll("--.*$", "");
                s = s.replaceAll("/\\*.*\\*/", "");
                sb.append(s);
            }
            br.close();

            String[] inst = sb.toString()
//                    .replaceAll("--.*\n", "")
//                    .replaceAll("/\\*.*\\*/", "")
                    .split(";");

            Connection c = getConnection();
            Statement st = c.createStatement();

            for(int i = 0; i<inst.length; i++) {
                if(!inst[i].trim().equals("")) {
                    st.executeUpdate(inst[i]);
                }
            }

        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    private <T> T safeCast(Object o, Class<T> clazz) {
        return clazz != null && clazz.isInstance(o) ? clazz.cast(o) : null;
    }

    @Override
    public List<Map<String, Object>> getItems(String query) {
        List<Map<String, Object>> items = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                Map<String, Object> item = new HashMap<>();
                int columnCount = resultSet.getMetaData().getColumnCount();
                String typeName;
                for(int index=1; index<=columnCount; index++) {
                    typeName = resultSet.getMetaData().getColumnTypeName(index);
                    String columnName = resultSet.getMetaData().getColumnName(index);
                    switch(typeName) {
                        case "NULL":
                            item.put(columnName, null);
                            break;
                        case "INTEGER":
                            item.put(columnName, resultSet.getInt(index));
                            break;
                        case "REAL":
                            item.put(columnName, resultSet.getFloat(index));
                            break;
                        case "BLOB":
                            item.put(columnName, resultSet.getBlob(index));
                            break;
                        case "BOOLEAN":
                            item.put(columnName, resultSet.getBoolean(index));
                            break;
                        case "STRING":
                        case "TEXT":
                        case "VARCHAR":
                            item.put(columnName, resultSet.getString(index));
                            break;
                        default:
                            item.put(columnName, resultSet.getObject(index));
                            break;
                    }
                }
                items.add(item);
            }
        } catch (SQLException e) {
            Logging.warning("Issue with running sqlite query: " + e.getMessage() + ", query: " + query);
            e.printStackTrace();
        }
        return items;
    }

    @Override
    public Map<String, Object> getItem(String query) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                Map<String, Object> map = new HashMap<>();
                int columnCount = resultSet.getMetaData().getColumnCount();
                for(int index=0; index<columnCount; index++) {
                    map.put(resultSet.getMetaData().getColumnName(index), resultSet.getObject(index));
                }
                return map;
            }
            else {
                return null;
            }
        } catch (SQLException e) {
            Logging.warning("Issue with running sqlite query: " + e.getMessage() + ", query: " + query);
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> T getItemColumns(String query, Class<T> type) {
        // TODO:
        Logging.error("GetItemColumns must be overriden!");
        return null;
    }

    @Override
    public <T> T getItemColumn(String query, Class<T> type) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
                String columnName = resultSetMetaData.getColumnName(1);

                Object column = resultSet.getObject(columnName);
                return safeCast(column, type);
            }
            else {
                return null;
            }
        } catch (SQLException e) {
            Logging.warning("Issue with running sqlite query: " + e.getMessage() + ", query: " + query);
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean query(String query) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            statement.executeQuery(query);
            return true;
        } catch (SQLException e) {
            Logging.warning("Issue with running sqlite query: " + e.getMessage() + ", query: " + query);
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void queryUpdate(String query) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException e) {
            Logging.warning("Issue with updating sqlite data: " + e.getMessage() + ", query: " + query);
            e.printStackTrace();
        }
    }


    @Override
    public int queryInsert(String query) {
        try {
            Connection connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            if (statement.executeUpdate() > 0) {
                ResultSet resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) {
                    int id = resultSet.getInt(1);
                    return id;
                }
            }
        } catch (SQLException e) {
            Logging.warning("Issue with inserting data to sqlite: " + e.getMessage() + ", query: " + query);
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void startTransaction() {
        queryUpdate("begin transaction");
    }

    @Override
    public void endTransaction() {
        queryUpdate("end transaction");
    }

    @Override
    public void rollback() {
        queryUpdate("rollback");
    }

    @Override
    public void reset() {
        // TODO:
        Logging.error("reset must be overriden!");
    }
}
