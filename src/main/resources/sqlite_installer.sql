-- Drop everything
DROP TABLE IF EXISTS directory;
DROP TABLE IF EXISTS bucket;
DROP TABLE IF EXISTS file;

-- Create tables
CREATE TABLE directory (
  id        INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  directory VARCHAR(255)                      NOT NULL UNIQUE,
  lastSync  INT(11)
);
CREATE TABLE bucket (
  id          INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  credentials VARCHAR(255), -- name of file containing authorization credentials
  clazz       VARCHAR(255) -- exact class name in java to be instantiated
);
CREATE TABLE file (
  id           INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  bucket       INTEGER,
  directory    INTEGER,
  path         VARCHAR(255),
  name         VARCHAR(255),
  synced       BOOLEAN                           NOT NULL DEFAULT FALSE,
  md5          VARCHAR(32), -- checksum of the file
  size         INTEGER,
  lastModified INTEGER,
  mimeType     VARCHAR(255),
  remoteId     VARCHAR(255),
  FOREIGN KEY (bucket) REFERENCES bucket (id),
  FOREIGN KEY (directory) REFERENCES directory (id)
);

-- Insert base data
INSERT INTO directory VALUES (NULL, "/home/bergice/Documents/Synced", NULL);
-- INSERT INTO directory VALUES (NULL, "/home/bergice/", NULL);
INSERT INTO bucket VALUES (NULL, "credentials_fragbucket", "GoogleDriveBucketEntity");
