# Fragcloud
Fragcloud lets you store as many files as you want on the cloud by linking multiple cloud storage accounts!

In the background, files will be allocated to different accounts (buckets), but what you see is one big cloud storage with your total quota accumulated.

## Features
- [ ] App
    - [ ] Register folders
    - [ ] Register buckets
- [ ] Framework
    - [ ] Clouds
        - [ ] GoogleDrive
            - [ ] Synchronize Remote Files (Downstream)
            - [X] Synchronize Local Files (Upstream)

## Guide
### Configuration
1. Setup your Google Drive credentials
    1. Create a new project [here](https://console.developers.google.com/flows/enableapi?apiid=drive)
    2. Click `Download JSON`
    3. Place the JSON file under `./src/main/resources/public/client_secret.json`
2. Run `fragcloud.Main.java`
3. Boom, free unlimited cloud storage!

### Command Line
There are various commands you can run to perform different actions:

#### Example
To set up a local folder to sync to a Google Drive account you must:
1. Add a remote `fragcloudcli addbucket --service GoogleCloud` (you will be prompted to authenticate)
2. Register a local folder `fragcloudcli addfolder "C:/MyFiles"`
3. Run the background sync service `fragcloud sync`

#### Commands

##### Management
Upload the database to remote:
```bash
fragcloudcli dbupload [bucket]
```

Download the database from remote:
```bash
fragcloudcli dbdownload [bucket]
```

Reset your database:
```bash
fragcloudcli dbreset
```

Add a bucket:
```bash
fragcloudcli addbucket [bucket]
```

Register a local folder:
```bash
fragcloudcli addfolder [path] [remotepath]
```

##### Storage
Sync local files to remote:
```bash
fragcloudcli uploadlocal
```

Sync remote files to local:
```bash
fragcloudcli downloadremote
```

## Tech Stack
- Java
- UI: JavaFX
- File Syncing: Java WatchService
- Clouds
    - Google Drive Java API v3
- Build: Maven